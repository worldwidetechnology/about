# about

World Wide Technology

## Summary
Founded in 1990, WWT has grown from a small product reseller into a technology solution provider with $10.4 billion in annual revenue and more than 5,000 employees. We serve the technology needs of large public and private organizations around the globe, including many of the world’s best-known brands